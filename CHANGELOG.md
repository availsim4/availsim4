2.2
===
- New probability laws: shifted exponential and exponentiated Weibull https://gitlab.cern.ch/availsim4/availsim4/-/issues/185
- RCA results presented in memory-efficient way https://gitlab.cern.ch/availsim4/availsim4/-/issues/184)
- Refactoring of the RCA code so that it doesn’t create overhead when not used (https://gitlab.cern.ch/availsim4/availsim4/-/issues/184)
- More resilient path definitions in HTCondor runner (for using AvailSim4 as a module and from other scripts) (https://gitlab.cern.ch/availsim4/availsim4/-/issues/182)
- Optional columns: all columns with default values no longer need to present (https://gitlab.cern.ch/availsim4/availsim4/-/issues/178)
- Phase duration statistics in RCA (https://gitlab.cern.ch/availsim4/availsim4/-/issues/184)
- AvailSim4 on CVMFS (https://gitlab.cern.ch/availsim4/availsim4/-/issues/166)
- Pyproject.toml instead of setup.py, fixed dependency requirements (https://gitlab.cern.ch/availsim4/availsim4/-/issues/181)
- Documentation improvements and example notebooks (https://gitlab.cern.ch/availsim4/availsim4/-/issues/160, https://gitlab.cern.ch/availsim4/availsim4/-/issues/181)
- Typo and minor bug fixes (e.g., https://gitlab.cern.ch/availsim4/availsim4/-/issues/175)

2.1.1
====
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/177

2.1.0
====
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/128
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/130
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/127
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/124
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/122
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/135
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/132
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/149
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/105
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/145
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/147
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/150
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/40
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/105
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/154
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/155
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/136
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/99
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/156
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/131
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/161
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/78
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/129
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/158
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/159
- https://gitlab.cern.ch/availsim4/availsim4/-/issues/94


2.0.2 (updated 2.0.1)
=====
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/114
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/115
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/116
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/124
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/126
- selected dependencies made optional due to installation difficulties in some environments

2.0.0
=====

- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/21
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/32
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/46
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/47
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/48
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/56
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/60
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/63
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/64
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/67
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/75
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/78
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/79
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/80
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/81
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/82
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/83
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/84
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/85
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/86
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/87
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/88
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/92
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/93
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/95
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/101
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/106
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/107
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/108

1.0.1
=====

- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/71

1.0.0
=====

- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/1
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/2
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/3
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/4
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/5
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/6
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/7
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/8
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/9
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/10
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/11
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/12
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/13
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/15
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/17
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/18
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/19
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/20
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/22
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/23
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/25
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/26
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/28
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/29
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/34
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/35
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/36
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/38
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/41
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/43
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/45
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/49
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/50
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/51
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/54
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/57
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/59
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/61
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/65
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/66
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/68
- https://gitlab.cern.ch/availsim4/availsim4core/-/issues/69
