# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright CERN 2021. All rights not expressly granted are reserved.

class ResultRecordEntry:

    def identifier(self):
        """
        This method defines an Identifier of the record entry.
        This is useful, in case we want to regroup the entry based on this identifier. {cf SimulationResults}
        """
        pass
